import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.events.message.guild.react.GuildMessageReactionAddEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.NotNull;

import javax.security.auth.login.LoginException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class Main extends ListenerAdapter {

    HashSet<User> users; //liste des gens qui ont déjà changé de rôle
    long messageId; //id du message à réact pour changer de rôle
    final static int messageDisplayTime = 10;

    //ID des rôles et salons
    final static long idPaysan = 496431820359860254L;
    final static long idNoble = 496431816190984229L;
    final static long idEsprit = 496433171349635073L;
    final static long idRelique = 498460513571831818L;

    //Roles
    Role paysan;
    Role noble;
    Role esprit;
    Role relique;

    public static void main (String[] args) throws LoginException {
        JDABuilder builder = JDABuilder.createDefault(args[0]);
        builder.addEventListeners(new Main());
        builder.setActivity(Activity.streaming("la fin du monde", "http://www.hasthelargehadroncolliderdestroyedtheworldyet.com/"));
        builder.build();
    }

    Main(){
        messageId = -1;
        users = new HashSet<>();
    }

    public static void deleteAfter(Message message, int delay) {
        message.delete().queueAfter(delay, TimeUnit.SECONDS);
    }

    public void setRoles(Guild guild){
        paysan = guild.getRoleById(idPaysan);
        noble = guild.getRoleById(idNoble);
        esprit = guild.getRoleById(idEsprit);
        relique = guild.getRoleById(idRelique);
    }

    public void testDependancies(MessageChannel channel){
        boolean missing = false;

        if (paysan == null) { channel.sendMessage("Role \"Paysan\" not found").queue(); missing = true; }
        if (noble == null) { channel.sendMessage("Role \"Noble\" not found").queue(); missing = true; }
        if (esprit == null) { channel.sendMessage("Role \"Esprit\" not found").queue(); missing = true; }
        if (relique == null) { channel.sendMessage("Role \"Relique\" not found").queue(); missing = true; }
        if (!missing) channel.sendMessage("All roles and channels found.").queue();
    }

    @Override
    public void onMessageReceived(@NotNull MessageReceivedEvent event) {
        String msg = event.getMessage().getContentRaw();
        MessageChannel channel = event.getChannel();

        if (msg.equalsIgnoreCase("il est temps de vieillir")){
            channel.sendMessage("Press F to become old").queue((message)-> messageId = message.getIdLong());
            setRoles(event.getGuild());
        }

        if (msg.equalsIgnoreCase("test passation bot")){
            setRoles(event.getGuild());
            testDependancies(channel);
        }

        //Pour le troll
        Random rand = new Random();
        int randint;

        if (Arrays.asList("ptdr t ki", "ptdr tki", "t ki", "tki").contains(msg.toLowerCase())) {
            randint = rand.nextInt(3);
            switch (randint) {
                case 0 -> channel.sendMessage("Toi t ki").queue();
                case 1 -> channel.sendMessage("Je suis ton père :fcklol:").queue();
                case 2 -> channel.sendMessage("mange tes morts").queue();
            }
        }

        if (msg.contains("quand la passation")) {
            randint = rand.nextInt(3);
            switch (randint) {
                case 0 -> channel.sendMessage("Bientôt").queue();
                case 1 -> channel.sendMessage("Minute papillon").queue();
                case 2 -> channel.sendMessage("Jamais").queue();
            }
        }

        if (msg.toLowerCase().contains("passation bot") && msg.toLowerCase().contains("mon avenir")) {
            Member member = event.getMember();

            if (member.getRoles().contains(paysan)) {
                randint = rand.nextInt(3);
                switch (randint) {
                    case 0 -> channel.sendMessage("Tu seras bientôt de droite").queue();
                    case 1 -> channel.sendMessage("Tu feras bientôt parti de la haute saucisse").queue();
                    case 2 -> channel.sendMessage("Tu auras de grands pouvoirs, et donc de grandes responsabilités").queue();
                }
            } else if (member.getRoles().contains(noble)) {
                randint = rand.nextInt(4);
                switch (randint) {
                    case 0 -> channel.sendMessage("Tu seras décapité.e dans peu de temps").queue();
                    case 1 -> channel.sendMessage("Tu vas subitement prendre un coup de vieux").queue();
                    case 2 -> channel.sendMessage("Tu vas bientôt devnir relou").queue();
                    case 3 -> channel.sendMessage("Tu vas rejoindre le panthéon des gens qui ont marqué le CJ à vie").queue();
                }

            } else if (member.getRoles().contains(esprit)) {
                randint = rand.nextInt(4);
                switch (randint) {
                    case 0 -> channel.sendMessage("Tu vas te désagréger dans les plus brefs délais").queue();
                    case 1 -> channel.sendMessage("Tu vas bientôt devenir encore plus relou").queue();
                    case 2 -> channel.sendMessage("Tu auras acquis une sagesse inégalée dans les semaines à venir").queue();
                    case 3 -> channel.sendMessage("Tu seras manqué.e de tous").queue();
                }
            } else if (member.getRoles().contains(relique)) {
                randint = rand.nextInt(3);
                switch (randint) {
                    case 0 -> channel.sendMessage("T'es pas déjà mort.e toi ?").queue();
                    case 1 -> channel.sendMessage("Il ne va rien se passer").queue();
                    case 2 -> channel.sendMessage("Tu vas rejoindre un musée").queue();
                }
            } else {
                randint = rand.nextInt(3);
                switch (randint) {
                    case 0 -> channel.sendMessage("Non").queue();
                    case 1 -> channel.sendMessage("Tu vas assister à de grands changements. Mais pas chez toi.").queue();
                    case 2 -> channel.sendMessage("Je parle pas aux touristes").queue();
                }
            }
        }
    }

    @Override
    public void onGuildMessageReactionAdd(@NotNull GuildMessageReactionAddEvent event) {
        if (messageId != -1 && event.getMessageIdLong() == messageId){

            //For debug
            //System.out.println(event.getReactionEmote().getAsCodepoints());

            if (event.getReactionEmote().getAsCodepoints().equals("U+1f1eb")){
                Member member = event.getMember();
                MessageChannel channel = event.getChannel();

                //Membre a déjà réagi au message
                if (users.contains(event.getUser())){
                    String text = event.getUser().getAsMention() + " Tas déjà grandi, ça suffit pour aujourd'hui";
                    channel.sendMessage(text).queue((msg) -> deleteAfter(msg, messageDisplayTime));
                }
                //Membre est déjà relique, pas besoin de changer son rôle
                else if (member.getRoles().contains(relique)) {

                    Random rand = new Random();
                    int randint = rand.nextInt(3);
                    String text = member.getUser().getAsMention();

                    switch (randint) {
                        case 0 -> text += " plus vieux que ça et c'est direction le cerceuil en fait";
                        case 1 -> text += " je vais devoir ajouter un rôle \"décrépi·e\" à ce stade là";
                        default -> text += " t'es trop vieux pour ces conneries";
                    }
                    channel.sendMessage(text).queue((msg) -> deleteAfter(msg, messageDisplayTime));
                }
                //Changement de rôle
                else {
                    String text = "";
                    Role roleToAdd = paysan, roleToRemove = paysan;

                    if (member.getRoles().contains(paysan)) {
                        roleToAdd = noble; roleToRemove = paysan;
                        text = event.getUser().getAsMention() + "paf t'es noble";
                    }
                    if (member.getRoles().contains(noble)) {
                        roleToAdd = esprit; roleToRemove = noble;
                        text = event.getUser().getAsMention() + "paf t'es vieille/vieux";
                    }
                    if (member.getRoles().contains(esprit)) {
                        roleToAdd = esprit; roleToRemove = relique;
                        text = event.getUser().getAsMention() + "paf t'es mort·e";
                    }

                    if (!text.isBlank()){
                        event.getGuild().addRoleToMember(member.getId(), roleToAdd).queue();
                        event.getGuild().removeRoleFromMember(member.getId(), roleToRemove).queue();

                        channel.sendMessage(text).queue((msg) -> deleteAfter(msg, messageDisplayTime));
                        users.add(member.getUser());
                    }
                }
            }

        }

    }
}
